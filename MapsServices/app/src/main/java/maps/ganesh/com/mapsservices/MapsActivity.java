package maps.ganesh.com.mapsservices;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity {

    static double lat,lng;
    private GoogleMap mMap;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
    }
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }
    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }
    private void setUpMap() {
        GPS gps=new GPS(this);
        lat=gps.getLatitude();
        lng=gps.getLongitude();
        LatLng loc=new LatLng(lat,lng);
        mMap.addMarker(new MarkerOptions().position(loc).title("Current Location"));
        LatLng locd=new LatLng(GeocodingLocation.dLat,GeocodingLocation.dLong);
        mMap.addMarker(new MarkerOptions().position(locd).title("Customer's Location"));
        double latMid=(lat+GeocodingLocation.dLat)/2;
        double lngMid=(lng+GeocodingLocation.dLong)/2;
        LatLng locMid=new LatLng(latMid,lngMid);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(locMid));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        String url = getMapsApiDirectionsUrl();
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);

    }

    private String getMapsApiDirectionsUrl() {
        String origin = lat + "," + lng;
        String key="AIzaSyDxsSMkdein6Bmqs5dEyrjGnuvn9IpUTxE";   //google maps api key
        String destination = GeocodingLocation.dLat + "," + GeocodingLocation.dLong ;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?origin=" +origin+ "&destination=" + destination+"&key="+key+"&alternatives=true";
        return url;
    }

    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;
            String distance = "";
            String duration = "";

            // traversing through routes
            try {
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<LatLng>();
                    polyLineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = routes.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        if(j==0){	// Get distance from the list
                            distance = (String)point.get("distance");
                            continue;
                        }else if(j==1){ // Get duration from the list
                            duration = (String)point.get("duration");
                            continue;
                        }

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    polyLineOptions.addAll(points);
                    polyLineOptions.width(8);
                    polyLineOptions.color(Color.BLUE);
                }
                mMap.addPolyline(polyLineOptions);
                textView=(TextView)findViewById(R.id.textView);
                textView.setText("Distance:" + distance + ", Duration:" + duration);
            }catch (NullPointerException e){

            }
        }
    }

}
